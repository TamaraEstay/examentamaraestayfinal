/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examentfinal.controllers;

import com.mycompany.examenfinal.dao.HistoricoDao;
import com.mycompany.examenfinal.modelo.HistoricoPalabras;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author arnaldoaraya
 */
@WebServlet(name = "ExamenFinalServlet", urlPatterns = {"/ExamenFinalServlet"})
public class ExamenFinalServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ExamenFinalServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ExamenFinalServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //aqui programar el servlet
        String url = "https://examentamaraestayfinal.herokuapp.com/api/dictionary/obtenerDefinicionPalabra/";
        HistoricoDao historicoDao = new HistoricoDao();
        switch(Integer.parseInt(request.getParameter("tipoSolicitud"))){
            case 1:
                String palabra = request.getParameter("palabra");
                HistoricoPalabras hp = new HistoricoPalabras();
                hp.setPalabra(palabra);
                hp.setFechaIngreso(new Date());
                List<HistoricoPalabras> totales = historicoDao.recuperarHistorial();
                hp.setCodigoHistorico((totales.size()+1));
                int resultadoCargaHistorico = historicoDao.agregarPalabraAHistorico(hp);//aqui almacenara el registro
                Client client = ClientBuilder.newClient();
                WebTarget myResource1 = client.target(url+palabra);
                String algo = myResource1.request(MediaType.APPLICATION_JSON).get(String.class);
                request.setAttribute("palabra", palabra);
                request.setAttribute("definicion", algo);
                break;
            case 2:
                List<HistoricoPalabras> totalesHistorico = historicoDao.recuperarHistorial();
                request.setAttribute("listaHistorial", totalesHistorico);
                break;
        }
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/index.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
