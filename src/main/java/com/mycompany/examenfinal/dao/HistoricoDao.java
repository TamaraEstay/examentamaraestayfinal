/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examenfinal.dao;

import com.mycompany.examenfinal.modelo.HistoricoPalabras;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author arnaldoaraya
 */
public class HistoricoDao {

    public int agregarPalabraAHistorico(HistoricoPalabras palabra) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
            EntityManager entityManager = emf.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(palabra);
            entityManager.getTransaction().commit();
            return 1;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }

    public List<HistoricoPalabras> recuperarHistorial() {
        List<HistoricoPalabras> historico = new ArrayList<>();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
            EntityManager entityManager = emf.createEntityManager();

            TypedQuery<HistoricoPalabras> consultarHistorico = entityManager.createNamedQuery("HistoricoPalabras.findAll", HistoricoPalabras.class);
            historico = consultarHistorico.getResultList();
        } catch (Exception e) {
            e.getMessage();
        }

        return historico;
    }
}
