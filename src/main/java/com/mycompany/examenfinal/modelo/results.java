    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examenfinal.modelo;

import java.util.List;

/**
 *
 * @author arnaldoaraya
 */
public class results {
 
    private String id, language;
    private List<lexicalEntries> lexicalEntries;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<lexicalEntries> getLexicalEntries() {
        return lexicalEntries;
    }

    public void setLexicalEntries(List<lexicalEntries> lexicalEntries) {
        this.lexicalEntries = lexicalEntries;
    }
}
